FROM sath89/oracle-ee-12c
MAINTAINER Pascal Peinecke

ENV workingDir apex-projekt
ENV version 0.1
ENV password datadata1337!

EXPOSE 3000

USER root

RUN /bin/bash -c 'sh /entrypoint.sh' &
RUN /bin/bash -c 'cd /root'

#RUN echo "root:" | chpasswd
#RUN setPassword.sh ${password}
RUN /bin/bash -c 'yum upgrade -y'

RUN /bin/bash -c 'curl https://transfer.sh/sfpNj/oracle-instantclient12.2-basic-12.2.0.1.0-1.x86_64.rpm -o /root/oracle-instantclient12.2-basic-12.2.0.1.0-1.x86_64.rpm'
RUN /bin/bash -c 'rpm -Uvh /root/oracle-instantclient12.2-basic-12.2.0.1.0-1.x86_64.rpm'
RUN /bin/bash -c 'curl https://transfer.sh/Jn8OF/oracle-instantclient12.2-sqlplus-12.2.0.1.0-1.x86_64.rpm -o /root/oracle-instantclient12.2-basic-12.2.0.1.0-1.x86_64.rpm'
RUN /bin/bash -c 'rpm -Uvh /root/oracle-instantclient12.2-basic-12.2.0.1.0-1.x86_64.rpm'
RUN /bin/bash -c 'curl https://transfer.sh/p7N5j/oracle-instantclient12.2-devel-12.2.0.1.0-1.x86_64.rpm -o /root/oracle-instantclient12.2-devel-12.2.0.1.0-1.x86_64.rpm'
RUN /bin/bash -c 'rpm -Uvh /root/oracle-instantclient12.2-devel-12.2.0.1.0-1.x86_64.rpm'

#RUN yum clean
#RUN /bin/bash -c 'export LD_LIBRARY_PATH=/usr/local/lib64/:$LD_LIBRARY_PATH'

RUN /bin/bash -c 'yum -y install mpfr-devel gmp-devel bzip2'

USER oracle
RUN /bin/bash -c 'cd /home/oracle'

RUN /bin/bash -c 'curl http://www.multiprecision.org/mpc/download/mpc-0.8.2.tar.gz -o /home/oracle/mpc-0.8.2.tar.gz'
RUN /bin/bash -c 'mkdir /home/oracle/mpc-0.8.2'
RUN /bin/bash -c 'tar zxvf /home/oracle/mpc-0.8.2.tar.gz -C /home/oracle/mpc-0.8.2'
RUN /bin/bash -c 'cd /home/oracle/mpc-0.8.2/mpc-0.8.2 && ./configure --disable-shared --enable-static --prefix=/tmp/gcc --with-gmp=/tmp/gcc --with-mpfr=/tmp/gcc'
RUN /bin/bash -c 'cd /home/oracle/mpc-0.8.2/mpc-0.8.2 && make'
RUN /bin/bash -c 'cd /home/oracle/mpc-0.8.2/mpc-0.8.2 && make check'

USER root
RUN /bin/bash -c 'cd /home/oracle/mpc-0.8.2/mpc-0.8.2 && make install'

USER root
RUN /bin/bash -c 'cd /root && curl --silent --location https://rpm.nodesource.com/setup_8.x | bash -'
RUN /bin/bash -c 'yum -y install nodejs'
RUN /bin/bash -c 'yum -y install git'
#RUN /bin/bash -c 'mkdir -p /opt/oracle/instantclient'
#RUN /bin/bash -c 'ln -s /usr/lib/oracle/12.2/client64/lib/libclntsh.so.12.1 /usr/lib/libclntsh.so'
#RUN /bin/bash -c 'ln -s /usr/lib/oracle/12.2/client64/lib/libclntsh.so.12.1 /usr/lib/libclntsh.so.12.1'
#RUN /bin/bash -c 'rm /usr/lib/oracle/12.2/client64/lib/libclntsh.so'
#RUN /bin/bash -c 'ln -s /usr/lib/oracle/12.2/client64/lib/libclntsh.so.12.1 /usr/lib/oracle/12.2/client64/lib/libclntsh.so'
#RUN /bin/bash -c 'ln -s /usr/lib/oracle/12.2/client64/lib/libclntsh.so.12.1 /opt/oracle/instantclient/libclntsh.so.12.1'
#RUN /bin/bash -c 'ln -s /usr/lib/oracle/12.2/client64/lib/libclntsh.so.12.1 /opt/oracle/instantclient/libclntsh.so'

#RUN /bin/bash -c 'rm /usr/lib/oracle/12.2/client64/lib/libclntsh.so && rm /opt/oracle/instantclient/libclntsh.so && ln -s /usr/lib/oracle/12.2/client64/lib/libclntsh.so.12.1 /opt/oracle/instantclient/libclntsh.so && ln -s /usr/lib/oracle/12.2/client64/lib/libclntsh.so.12.1 /usr/lib/oracle/12.2/client64/lib/libclntsh.so && rm /opt/oracle/instantclient/libclntsh.so.12.1 && ln -s /usr/lib/oracle/12.2/client64/lib/libclntsh.so.12.1 /opt/oracle/instantclient/libclntsh.so.12.1'
#RUN /bin/bash -c 'mkdir -p /opt/oracle/instantclient/sdk/include'
#RUN /bin/bash -c 'curl https://transfer.sh/R8BaY/oci.h -o /opt/oracle/instantclient/sdk/include/oci.h'

USER root
RUN /bin/bash -c 'cd /root'
RUN /bin/bash -c 'git clone https://github.com/Pascal3366/apex-projekt /root/apex-projekt'
RUN /bin/bash -c 'rm -rf /root/apex-projekt/node_modules'

RUN /bin/bash -c 'export LD_LIBRARY_PATH=/usr/lib/oracle/12.2/client64/lib && export OCI_DIR=/usr/lib/oracle/12.2/client64 && export OCI_LIB_DIR=/usr/lib/oracle/12.2/client64/lib && export OCI_INC_DIR=/usr/include/oracle/12.2/client64 && cd /root/apex-projekt && npm install && npm install mime && npm start'
#export OCI_DIR=/usr/lib/oracle/12.2/client64 && export OCI_LIB_DIR=/opt/oracle/instantclient && export OCI_INC_DIR=/opt/oracle/instantclient/sdk/include'
#RUN /bin/bash -c 'ls $LD_LIBRARY_PATH/'
# RUN /bin/bash -c 'ls $OCI_DIR/'
# RUN /bin/bash -c 'ls $OCI_LIB_DIR/'
# RUN /bin/bash -c 'ls $OCI_INC_DIR/'
#RUN /bin/bash -c 'cd /home/oracle/apex-projekt && export LD_LIBRARY_PATH=/usr/lib/oracle/12.2/client64:$LD_LIBRARY_PATH && export OCI_DIR=/usr/lib/oracle/12.2/client64 && export OCI_LIB_DIR=/opt/oracle/instantclient && export OCI_INC_DIR=/opt/oracle/instantclient/sdk/include && rm -rf node_modules && npm install && npm start'
